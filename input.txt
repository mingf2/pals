################################# General Rules ############################################

	# (1) '#' stands for comments.
	# (2) blank line will be ignored.
	# (3) tabs and spaces at the beginning of a line will be ignored.

############################################################################################

################################# File Path ################################################
# the path consists of several parts.
# File path = DIRECTORY + '/' + FOLDERS[i] + '/' + CHANNELS[j] + ".bin"

	# Directory
		#Directory: /media/ming/INL_2016/CLYC&CLLBC/CLYC&CLLBC_test_07.05.19/1DAQ/Cs_2/2DAQ/run/FILTERED
		#Directory: /media/ming/SeagateDrive/06132019_pube/DAQ/run_preamponlyPuBe/FILTERED
		#Directory: /media/ming/SeagateDrive/06132019/DAQ/run_preamponly/FILTERED
		#Directory: /media/ming/SeagateDrive/032119/Co60_23/FILTERED
		#Directory: /media/ming/SeagateDrive/active_interrogation/day1/active_run_measurement1/active_run_7/FILTERED
		#Directory: /media/ming/SeagateDrive/active_interrogation/day2/active_25rods_day2_1h_good/FILTERED
		#Directory: /media/ming/SeagateDrive/051319/DAQ/na22_quartz_3/FILTERED
		#Directory: /media/ming/SeagateDrive/PALS_more_data/na22_quartz_day1_14h_1/FILTERED
		#Directory: /media/ming/SeagateDrive/PALS_more_data/na22_quartz_day2_24h/FILTERED
		#Directory: /media/ming/SeagateDrive/PALS_more_data/na22_quartz_day3_24h_3/FILTERED
		#Directory: /media/ming/SeagateDrive/PALS_more_data/na22_quartz_day4_8h_2/FILTERED
		#Directory: /media/ming/SeagateDrive/PALS_more_data/na22_quartz_day4_16h_4/FILTERED
		#Directory: /media/ming/SeagateDrive/PALS_more_data/na22_quartz_day5_24h_1/FILTERED
		#Directory: /media/ming/SeagateDrive/PALS_more_data/na22_quartz_day6_24h_1/FILTERED
		#Directory: /media/ming/SeagateDrive/PALS_more_data/na22_quartz_day7_24h_1/FILTERED
		#Directory: /media/ming/SeagateDrive/PALS_more_data/na22_quartz_day8_24h_2/FILTERED
		#Directory: /media/ming/SeagateDrive/generator_test/Gamma_30kHz/FILTERED/0
		Directory: /media/ming/SeagateDrive/generator_test/Neutron_10ns_3kHz/FILTERED

	# Folders: two comma-separated integers or one integer.
		# example: 10 folders from No.0 to No.9: 0, 9
		#	  		only 1 folder No.1: 1
	Folders: 0

	# Channels: specify the channels to process
		# example: Channel 0 and Channel 4: 0, 4
	Channels: 0

	# Filetype
		# example: binary, FILETYPE = 0;
		#	  		ASCII, FILETYPE = 1; 
	Filetype: 0

	# Maximum number of pulses to process
	MaxNumPulses: 1000000

############################################################################################

################################## Input Settings ##########################################

#Data Format

NHeaders: 7 # number of headers in one pulse
Headersize: 2, 2, 8, 2, 2, 4, 4 # size of each header in bytes
	# for CoMPASS, the headers are: 
	# Board, Channel, Time Stamps, Energy(ch), Energy Short(ch), Flags, Number of Samples
NSamples: 200 # number of samples in one pulse
Samplesize: 2 # size of each sample in bytes
Dynamicrange: 2.0 # unit: Volt
Resolultion: 14 #12 bits
Offset: 8 # number of samples needed to calculate the baseline
Delt: 2 #2 # sampling time, unit: ns
SavePulses: 0 # if '1', save the pulses

##############################################################################################

####################################### Coincidence ###########################################

# if Coincidence = 1, use the time stamp in the header to find the coincidences
# if coincidence = 0, skip

Coincidence: 0
	# Channels
	Coincidence Channels: 0, 1
	Time Window: 200 #ns
		# if DT < Time Window, we find a pair of coincident events.
	SaveDT: 1 # whether to save the time differences
	TOF: 1 # whether to plot the TOF spectrum
	# if TOF = 1
		SaveTOF: 1 # whether to save the TOF spectrum

################################################################################################

####################################### Energy Calibration #####################################

# specify the calibration coefficents, unit: keVee/V
# the number of coefficents should equal to the number of channels.
# energy = total integral * calibration coefficient
# the calibration coefficient is calculated using a Matlab script.
Calibration: 355 #153.5496, 158.0688 #88.9137, 89.0628 # 110.9563, 110.1382 #152.0356, 151.746

################################################################################################

####################################### Pulse Rejection ########################################

# Bad pulses 
Filter bad: 1
	# if filter bad = 1
	Zerosupression: 1
		# if Zerosupression = 1, reject the pulses whose amplitudes < MinVoltage
		MinVoltage: 0.0 #0.8 # unit: Volt
	Clipped: 1
		# if Zerosupression = 1, reject the clipped pulses.
		MaxVoltage: 2.0 # unit: Volt
	Save bad: 0
		# if Save bad = 1, save the first 100 bad pulses.

# Piled-up pulses
Filter piled-up: 1
	# If filter piled-up =1
	Save piled-up: 0
	Save S_time: 0
    Save S_frequency: 0

# Energy cut
# if EnergyCut = 1, only process the pulses whose energy is between EnergyLow and EnergyHigh
EnergyCut: 0
EnergyLow: 200 # unit: keVee
EnergyHigh: 2000 # unit: keVee

#PSD cut
# if PSDCut = 1, only process the pulses with PSD between PSDLow and PSDHigh
PSDCut: 0, 0
PSDLow: 0, 0.2
PSDHigh: 1, 0.35
################################################################################################

####################################### Calculate the time stamps ##############################

Time stamp: 0
	# if time stamp =1, calculate time stamps using DIACFD
	Interpolation: 7 # number of interpolation points to add between two samples
	TRise: 20 # rough estimate of rise time in number of samples
	#DIACFD parameters
		Fraction: 0.4 # range from 0 to 1.
		Filter Width: 1
		Time Delay: 4 #16 # unit: ns
    SaveTimeStamp: 0 # if '1', save the time stamps of good pulses

################################################################################################

####################################### Plots ##################################################		

# if SavePH = 1, save the pulse heights
SavePH: 0

PHD: 0
	# if PHD = 1, plot the pulse height distribution
	SavePHD: 1
	PHDBins: 200
	PHmin: 0
	PHmax: 0 #volts # if 0, automatically set the plot range of PID

# if SavePH = 1, save the pulse integrals
SavePI: 0

PID: 1
	# if PID = 1, plot the pulse integral distribution
	# pulse total integral = sum of all samples with index between PreGate and LongGate.
	SavePID: 0
	PIDBins: 200
	PImin: 0 
	PImax: 2000 # if 0, automatically set the plot range of PID
	PreGate: 10 # ns
	LongGate: 200 # ns
PSD: 1
	# if '1', plot tail integral / total integral vs total integral
	# pulse tail integral = sum of all samples with index between PreGate and ShortGate.
	ShortGate: 40 #60 # ns
	SaveIntegrals: 0
	## plot options
	PSDXBins: 200
	PSDXmin: 0
	PSDXmax: 2000
	PSDYBins: 200
	PSDYmin: 0 
	PSDYmax: 1
	

################################################################################################